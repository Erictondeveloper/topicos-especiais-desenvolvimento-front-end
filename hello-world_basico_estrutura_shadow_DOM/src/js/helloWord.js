class HelloWordElement extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({mode: "open"});
    //Shadow DOM
    const span = document.createElement("span");
    span.textContent = "Hello-Word!";
    this.shadowRoot.append(span);
  }
  //
}
customElements.define("hello-world", HelloWordElement);