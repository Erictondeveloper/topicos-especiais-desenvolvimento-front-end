class ListaColorida extends HTMLUListElement {
  constructor(){
    super();
    console.log("ListaColorida");

    const lis = this.querySelectorAll("li");
    console.log(lis.length);
    for (let i = 0; i < lis.length; i++){
      lis[i].style.color = `hsl(${(i / lis.length) * 360}, 100%, 50%)`;
    }
  }
  //
}
customElements.define(
  "lista-colorida", 
  ListaColorida, 
  {extends: "ul"}
);