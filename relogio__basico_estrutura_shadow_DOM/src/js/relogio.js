// A calsse WcRelogio precisa ser subclass de HTMLElement
class WcRelogio extends HTMLElement {
    static observedAttributes = ["tipo"];

    constructor(){

        const date = new Date();
        super();
        this.attachShadow({mode: 'open'});
        const style = document.createElement("style");
        style.textContent = `
        :host {
            display: inline-block;
            font-family: Arial;
        }`;
        this.shadowRoot.append(style);
        this.elementoPrincipal = document.createElement("div");
        this.shadowRoot.append(this.elementoPrincipal);
    }

    connectedCallback() {
        this.analogico = this.getAttribute("tipo") === "analogico";
        this.exibeHora();
        this.interval = setInterval(() => {
            this.exibeHora();
        }, 500);
    }

    disconnectedCallback() {
        clearInterval(this.interval);
    }

    attributeChangedCallback(){
        this.analogico = this.getAttribute("tipo") === "analogico";
        this.exibeHora();
    }

    exibeHora() {
        if(this.analogico) {
            this.exibeHoraAnalogico();
        } else {
            this.exibeHoraDigital();
        }
    }

    exibeHoraDigital() {
        const date = new Date();
        this.elementoPrincipal.textContent = formataHora(date);
    }

    exibeHoraAnalogico() {
        const date = new Date();
        const s = date.getSeconds();
        const m = date.getMinutes() + (s /60);
        const h = date.getHours() % 12 + (m / 60);

        let marcadores = "";

        for ( let i = 0; i < 12; i++) {
            marcadores += `<path transform="rotate(${i * 30})" d="M 0 -42 v -3" /> \n`
        }

        this.elementoPrincipal.innerHTML = `
        <svg width+"100" height="100">
            <g transform="translate(50, 50)"  stroke="black" fill="none" stroke-width="2">
                <circle cx="0" cy="0" r="45" />
                ${marcadores}

                <g transform="rotate(${h * (360/12)})">
                    <path d="M 0 0 v -35" stroker-width="3" />
                </g>
                <g transform="rotate(${m * (360/60)})">
                    <path d="M 0 0 v -40" />
                </g>
                <g transform="rotate(${s * (360/60)})">
                    <path d="M 0 0 v -40" stroke="red" />
                    <circle cx="0" cy="0" r="3" fill="red" stroke="none" />
                </g>
            </g>  
        </svg>`
    }
}

function formataHora (date) {
   return date.toLocaleTimeString();
}
customElements.define("wc-relogio", WcRelogio);